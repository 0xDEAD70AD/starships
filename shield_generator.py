#
# The shield generator is what generates the shield
#   for the ship to use.  Like everything on a starship,
#   the generator will need to consume power.  Should
#   it fail to find the power it needs, it will shut the
#   shield down.
#
# Class constructor takes in a shield object.  A shield
#   that is powered by the generator can not have a greater
#   shield capacity than the generator can provide.
#

import mitigation_shields.py

class ShieldGenerator:
    power_consumption = None
    shield_capacity = None
    generator_health_full = None
    generator_health_integrity = None
    shield_repair_efficiency = None
    shield = None


    def __init__(self, power_consumption: int, shield_capacity: int, shield: MitigationShield,
                 hp: int, efficiency: int):
        self.power_consumption = power_consumption
        self.shield_capacity = shield_capacity
        self.generator_health_full = hp
        self.generator_health_integrity = self.generator_health_full
        self.shield_repair_efficiency = efficiency
        self.shield = shield
        self.shield.set_capacity(self.shield_capacity)

    # Regenerates shield incrementally when called.
    # Ability based on how damaged generator is.
    def shield_repair(self):
        efficiency = (self.generator_health_integrity // self.generator_health_full) / 100
        self.shield.shield_repair(self.shield_repair_efficiency * efficiency)

    def generator_damage(self, damage: int):
