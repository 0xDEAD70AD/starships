#
# This is a class for the mitigation shields.
# Mitigation shields are shields that have their own
#   capacity (like HP in an RPG).
# They also have a mitigation factor, which is
#   represented by a percent.  The most advanced
#   mitigation shields, while undamaged, have a
#   mitigation factor of 97-99 %.  With the exception
#   of maximum value, mitigation factor is correlated
#   to percent of capacity.
# As the shield absorbs damage, and its own capacity
#   is diminished, their capacity goes down.
# The amount of damage that bleeds through the shields
#   and into the ship itself is 100% - mitigation_factor.
#
# There may exist some weapons which are designed to
#   penetrate the mitigation shields by more than
#   their mitigation factor would otherwise allow.
#


class MitigationShield:
    capacity = None
    capacity_integrity = None
    mitigation_factor = None
    mitigation_integrity = None

    def __init__(self, cap: int, mf: int):
        self.capacity = cap
        self.capacity_integrity = self.capacity
        self.mitigation_factor = mf
        self.mitigation_integrity = self.mitigation_factor

    # To be called any time the shield is repaired or it
    #   takes any damage.
    def mitigation_adjustment(self):
        potential_mitigation = self.capacity_integrity // self.capacity
        if potential_mitigation > self.mitigation_factor:
            self.mitigation_integrity = self.mitigation_factor
        else:
            self.mitigation_integrity = potential_mitigation

    def shield_repair(self, repair_value: int):
        if self.capacity_integrity + repair_value < self.capacity:
            self.capacity_integrity += repair_value
        else:
            self.capacity_integrity = self.capacity

        mitigation_adjustment()

    # Should be called with one or two arguments.
    # If two, the first is damage and the second is
    #   penetration (an int representing a percent.
    # If one, only damage.
    def shield_damage(*args):
        damage = args[0]
        if len(args) == 2:
            penetration = args[1]
        else:
            penetration = 0

        bleedthrough_percent = (100 - (self.capacity_integrity - penetration)) / 100
        bleedthrough_damage = damage * bleedthrough_percent

        shield_damage = damage - bleedthrough_damage

        self.capacity_integrity -= shield_damage

        mitigation_adjustment()

        # This is passed on to the hull and/or systems
        return bleedthrough_damage

    # Returns status information
    def report(self):
        print("Shield capacity at " + self.capacity_integrity + " of " + self.capacity + ".")
        print("Shield mitigation factor at " + self.mitigation_integrity + " percent.")

    def set_capacity(self, cap: int):
        self.capacity = cap

    def get_capacity(self):
        return self.capacity

    def get_capacity_integrity(self):
        return self.capacity_integrity

    def get_mitigation_factor(self):
        return self.mitigation_factor

    def get_mitigation_integrity(self):
        return self.mitigation_integrity